package com.iut.lannion.graph.routing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.iut.lannion.graph.TestGraphFactory;
import com.iut.lannion.graph.model.Edge;
import com.iut.lannion.graph.model.Graph;
import com.iut.lannion.graph.routing.DijkstraPathFinder;

/**
 * Tests fonctionnels sur DijkstraPathFinder
 * 
 * @author etassel
 *
 */
public class DijkstraRegressTest {

	private Graph graph;

	private DijkstraPathFinder finder;

	@Before
	public void setUp() throws Exception {
		this.graph = TestGraphFactory.createGraph01();
		this.finder = new DijkstraPathFinder(graph);
	}

	@Test
	public void testABFound() {
		List<Edge> path = finder.findPath(graph.findVertex("a"), graph.findVertex("b"));
		assertNotNull(path);
		assertEquals(1, path.size());
	}

	@Test
	public void testBANotFound() {
		List<Edge> path = finder.findPath(graph.findVertex("b"), graph.findVertex("a"));
		assertNull(path);
	}

	@Test
	public void testACFoundWithCorrectOrder() {
		List<Edge> path = finder.findPath(graph.findVertex("a"), graph.findVertex("c"));
		assertNotNull(path);
		assertEquals(2, path.size());

		int index = 0;
		{
			Edge edge = path.get(index++);
			assertEquals("a", edge.getSource().getId());
			assertEquals("b", edge.getTarget().getId());
		}
		{
			Edge edge = path.get(index++);
			assertEquals("b", edge.getSource().getId());
			assertEquals("c", edge.getTarget().getId());
		}
	}
}
